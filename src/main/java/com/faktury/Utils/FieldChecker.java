package com.faktury.Utils;

import java.util.Collection;

public class FieldChecker {

    public static boolean isNotEmpty(Object value) {
        return !isEmpty(value);
    }

    public static boolean isEmpty(Object value) {
        return value == null
                || ((value instanceof String) && ((String) value).isEmpty())
                || ((value instanceof Collection) && ((Collection) value).isEmpty());
    }

    public static boolean matchesProductImagePattern(String filename) {
        return filename.matches("[0-9]{1,2}[_][a-zA-Z_]*[_][0-9]");
    }

}
