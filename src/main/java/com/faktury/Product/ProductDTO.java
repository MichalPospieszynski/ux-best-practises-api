package com.faktury.Product;

import java.math.BigDecimal;
import java.util.List;

public class ProductDTO {
    private Long id;
    private String name;
    private String style;
    private String fabric;
    private String fashion;
    private byte[] brandLogo;
    private BigDecimal price;
    private List<ProductImageDTO> images;
    private Long categoryId;

    public Long getId() {
        return id;
    }

    public ProductDTO setId(Long id) {
        this.id = id;
        return this;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public ProductDTO setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
        return this;
    }

    public String getName() {
        return name;
    }

    public ProductDTO setName(String name) {
        this.name = name;
        return this;
    }

    public String getStyle() {
        return style;
    }

    public ProductDTO setStyle(String style) {
        this.style = style;
        return this;
    }

    public String getFabric() {
        return fabric;
    }

    public ProductDTO setFabric(String fabric) {
        this.fabric = fabric;
        return this;
    }

    public String getFashion() {
        return fashion;
    }

    public ProductDTO setFashion(String fashion) {
        this.fashion = fashion;
        return this;
    }

    public byte[] getBrandLogo() {
        return brandLogo;
    }

    public ProductDTO setBrandLogo(byte[] brandLogo) {
        this.brandLogo = brandLogo;
        return this;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public ProductDTO setPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    public List<ProductImageDTO> getImages() {
        return images;
    }

    public ProductDTO setImages(List<ProductImageDTO> images) {
        this.images = images;
        return this;
    }
}
