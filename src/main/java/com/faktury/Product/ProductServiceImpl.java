package com.faktury.Product;

import com.faktury.Category.Category;
import com.faktury.Category.CategoryRepository;
import com.faktury.ProductColor.ProductColor;
import com.faktury.ProductColor.ProductColorRepository;
import com.faktury.ProductImage.ProductImage;
import com.faktury.ProductImage.ProductImageRepository;
import com.faktury.Utils.FieldChecker;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.ResourcePatternUtils;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static com.faktury.Utils.FieldChecker.isEmpty;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private ProductColorRepository productColorRepository;
    @Autowired
    private ProductImageRepository productImageRepository;
    @Autowired
    private ResourceLoader resourceLoader;

    @Override
    public Set<ProductDTO> findByCategoryId(ProductFilter productFilter) {
        Category category = productFilter.categoryId == null ? null : categoryRepository.findOne(productFilter.categoryId);
        Set<Product> products = new HashSet<>(category == null ? productRepository.findAll() : productRepository.findByCategory(category));
        if(category != null) {
            category.getSubCategories().forEach( sub -> {
                products.addAll(productRepository.findByCategory(sub));
                sub.getSubCategories().forEach( subSub -> {
                    products.addAll(productRepository.findByCategory(subSub));
                });
            });
        }
        Comparator comparator = Comparator.comparing(ProductDTO::getPrice);
        if(productFilter.sortOrder != null) {
            switch (productFilter.sortOrder) {
                case "Cena rosnąco":
                    comparator = Comparator.comparing(ProductDTO::getPrice);
                    break;
                case "Cena malejąco":
                    comparator = Comparator.comparing(ProductDTO::getPrice).reversed();
                    break;
                case "Alfabetycznie od A do Z":
                    comparator = Comparator.comparing(ProductDTO::getName);
                    break;
                case "Alfabetycznie od Z do A":
                    comparator = Comparator.comparing(ProductDTO::getName).reversed();
                    break;
                default:
                    break;
            }
        }
        return (Set<ProductDTO>) products.stream().map(this::mapToProductDTO).sorted(comparator).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    @Override
    public Set<String> getColors() {
        return productColorRepository.findAll().stream().map(ProductColor::getName).collect(Collectors.toSet());
    }

    @Override
    public void prepareData() throws IOException {
        productImageRepository.deleteAll();
        productColorRepository.deleteAll();

        List<File> productsImages = new ArrayList<>();
        List<File> brandsImages = new ArrayList<>();

        List<Resource> resources = Arrays.asList(ResourcePatternUtils.getResourcePatternResolver(resourceLoader).getResources("classpath:images/*.jpg"));

        resources.forEach( it -> {
            if (FieldChecker.matchesProductImagePattern(it.getFilename().split("\\.")[0])) {
                try {
                    productsImages.add(it.getFile());
                } catch (IOException ignored) { }
            } else {
                try {
                    brandsImages.add(it.getFile());
                } catch (IOException ignored) { }
            }
        });

        processProductsImages(productsImages);
        processBrandsImages(brandsImages);

    }

    private void processProductsImages(List<File> productsImages) {
        productsImages.forEach(this::processProductImage);
    }

    private void processProductImage(File productImageFile) {
        String fileName = productImageFile.getName().split("\\.")[0];
        String[] fileNameSplit = fileName.split("_");
        String productId = fileNameSplit[0];
        String order = fileNameSplit[fileNameSplit.length-1];
        String color = String.join(" ", Arrays.asList(fileNameSplit).subList(1, fileNameSplit.length - 1));
        Product product = productRepository.findOne(Long.valueOf(productId));
        ProductColor productColor = productColorRepository.findByProductAndName(product, color);
        if(productColor == null) {
            productColor = new ProductColor()
                    .setProduct(product)
                    .setName(color);
            productColor = productColorRepository.save(productColor);
        }
        try {
            ProductImage productImage = new ProductImage()
                    .setContent(IOUtils.toByteArray(new FileInputStream(productImageFile)))
                    .setOrder(Integer.valueOf(order))
                    .setProductColor(productColor);
            productImageRepository.save(productImage);
        } catch (IOException e) {
            System.out.println("Failed to save ProductImage: "+e.getMessage());
        }
    }

    private void processBrandsImages(List<File> brandsImages) {
        List<Product> products = productRepository.findAll();
        products.forEach(it -> {
            try {
                fillWithBrandImage(it, brandsImages);
            } catch (IOException e) {
                System.out.println("Failed to fill product with brand logo: "+e.getMessage());
            }
        });
    }

    private void fillWithBrandImage(Product product, List<File> brandsImages) throws IOException {
        product.setBrandLogo(IOUtils.toByteArray(new FileInputStream(brandsImages.get(new Random().nextInt(brandsImages.size()-1)))));
        productRepository.save(product);
    }

    @Override
    public ProductDTO mapToProductDTO(Product product) {
        return new ProductDTO()
                .setId(product.getId())
                .setName(product.getName())
                .setStyle(product.getStyle())
                .setFabric(product.getFabric())
                .setFashion(product.getFashion())
                .setBrandLogo(product.getBrandLogo())
                .setPrice(product.getPrice())
                .setImages(getProductsImages(product))
                .setCategoryId(product.getCategory().getId());
    }

    private List<ProductImageDTO> getProductsImages(Product product) {
        List<ProductImageDTO> productImageDTOS = new ArrayList<>();
        product.getProductColors().forEach( pc -> {
            pc.getProductImages().forEach( pi -> {
                productImageDTOS.add(new ProductImageDTO()
                        .setColor(pc.getName())
                        .setOrder(pi.getOrder())
                        .setContent(pi.getContent())
                );
            });
        });
        productImageDTOS.sort((Comparator.comparing(ProductImageDTO::getOrder)));
        return productImageDTOS;
    }
}