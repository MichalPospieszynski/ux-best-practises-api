package com.faktury.Product;

import java.util.List;

public class ProductFilter {

    public Long categoryId;
    public Long currentPage;
    public Long pageSize;
    public String sortOrder;
    public List<String> sizes;
    public List<String> colors;
    public Long priceFrom;
    public Long priceTo;

}
