package com.faktury.Product;

public class ProductImageDTO {

    private Integer order;
    private String color;
    private byte[] content;

    public Integer getOrder() {
        return order;
    }

    public ProductImageDTO setOrder(Integer order) {
        this.order = order;
        return this;
    }

    public String getColor() {
        return color;
    }

    public ProductImageDTO setColor(String color) {
        this.color = color;
        return this;
    }

    public byte[] getContent() {
        return content;
    }

    public ProductImageDTO setContent(byte[] content) {
        this.content = content;
        return this;
    }
}
