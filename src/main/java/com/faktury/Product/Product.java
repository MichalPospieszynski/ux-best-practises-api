package com.faktury.Product;


import com.faktury.Category.Category;
import com.faktury.ProductColor.ProductColor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Set;

@Entity
@Table(name = "product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "prd_id")
    private Long id;

    @Column(name = "prd_name")
    private String name;

    @Column(name = "prd_style")
    private String style;

    @Column(name = "prd_fabric")
    private String fabric;

    @Column(name = "prd_fashion")
    private String fashion;

    @Column(name = "prd_brand_logo")
    private byte[] brandLogo;

    @Column(name = "prd_price")
    private BigDecimal price;

    @ManyToOne
    @JoinColumn(name = "prd_category")
    private Category category;

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    private Set<ProductColor> productColors;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getFabric() {
        return fabric;
    }

    public void setFabric(String fabric) {
        this.fabric = fabric;
    }

    public String getFashion() {
        return fashion;
    }

    public void setFashion(String fashion) {
        this.fashion = fashion;
    }

    public byte[] getBrandLogo() {
        return brandLogo;
    }

    public void setBrandLogo(byte[] brandLogo) {
        this.brandLogo = brandLogo;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Set<ProductColor> getProductColors() {
        return productColors;
    }
}