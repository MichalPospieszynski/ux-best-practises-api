package com.faktury.Product;


import java.io.IOException;
import java.util.Set;

public interface ProductService {
    Set<ProductDTO> findByCategoryId(ProductFilter productFilter);
    Set<String> getColors();
    void prepareData() throws IOException;
    ProductDTO mapToProductDTO(Product product);
}