package com.faktury.Product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/products")
public class ProductController {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService= productService;
    }

    @RequestMapping(
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<?> getByCategoryId(@RequestBody ProductFilter productFilter){
        return ResponseEntity.ok(productService.findByCategoryId(productFilter));
    }

    @RequestMapping(
            value = "/colors",
            method = RequestMethod.GET
    )
    public ResponseEntity<?> getColors() {
        try {
            return ResponseEntity.ok(productService.getColors());
        } catch (Exception ex) {
            return ResponseEntity.ok(ex.getMessage());
        }
    }

    @RequestMapping(
            value = "/prepare",
            method = RequestMethod.GET
    )
    public ResponseEntity<?> prepareData() {
        try {
            productService.prepareData();
            return ResponseEntity.ok("Dane gotowe");
        } catch (Exception ex) {
            return ResponseEntity.ok(ex.getMessage());
        }
    }

}
