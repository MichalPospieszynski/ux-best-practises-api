package com.faktury.ProductImage;


import com.faktury.Product.Product;
import com.faktury.ProductColor.ProductColor;

import javax.persistence.*;

@Entity
@Table(name = "product_image")
public class ProductImage {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "pri_id")
    private Long id;

    @Column(name = "pri_content")
    private byte[] content;

    @Column(name = "pri_order")
    private Integer order;

    @ManyToOne
    @JoinColumn(name = "pri_color")
    private ProductColor productColor;

    public Long getId() {
        return id;
    }

    public ProductImage setId(Long id) {
        this.id = id;
        return this;
    }

    public Integer getOrder() {
        return order;
    }

    public ProductImage setOrder(Integer order) {
        this.order = order;
        return this;
    }

    public byte[] getContent() {
        return content;
    }

    public ProductImage setContent(byte[] content) {
        this.content = content;
        return this;
    }

    public ProductColor getProductColor() {
        return productColor;
    }

    public ProductImage setProductColor(ProductColor productColor) {
        this.productColor = productColor;
        return this;
    }
}