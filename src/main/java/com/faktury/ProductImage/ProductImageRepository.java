package com.faktury.ProductImage;

import com.faktury.Product.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface ProductImageRepository extends JpaRepository<ProductImage, Long> {



}