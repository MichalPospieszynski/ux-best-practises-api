package com.faktury.ProductColor;

import com.faktury.Product.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface ProductColorRepository extends JpaRepository<ProductColor, Long> {

    ProductColor findByProductAndName(Product product, String name);

}