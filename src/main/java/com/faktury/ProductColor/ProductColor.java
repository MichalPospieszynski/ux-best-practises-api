package com.faktury.ProductColor;


import com.faktury.Category.Category;
import com.faktury.Product.Product;
import com.faktury.ProductImage.ProductImage;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "product_color")
public class ProductColor {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "prc_id")
    private Long id;

    @Column(name = "prc_name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "prc_product")
    private Product product;

    @OneToMany(mappedBy = "productColor", cascade = CascadeType.ALL)
    private Set<ProductImage> productImages;

    public Long getId() {
        return id;
    }

    public ProductColor setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public ProductColor setName(String name) {
        this.name = name;
        return this;
    }

    public Product getProduct() {
        return product;
    }

    public ProductColor setProduct(Product product) {
        this.product = product;
        return this;
    }

    public Set<ProductImage> getProductImages() {
        return productImages;
    }
}