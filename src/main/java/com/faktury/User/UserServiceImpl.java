package com.faktury.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    UserRepository userRepository;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    AuthorityRepository authorityRepository;

    @Override
    public List<UserDTO> getUsers() {
        List<User>  users = userRepository.findAll();
        List<UserDTO> userDTOS = new ArrayList<>();
        users.forEach(user -> userDTOS.add(new UserDTO(user)));
        return userDTOS;
    }

    @Override
    public User findByEmail(String email){
        return userRepository.findByEmail(email);
    }

    @Override
    public boolean registerUser(RegisterForm registerForm) {
        User user = userRepository.findByEmail(registerForm.getUsername());
        if(user == null) {
            Authority auth = authorityRepository.findByName("ROLE_USER");
            userRepository.save(new User(registerForm.getUsername(), passwordEncoder.encode(registerForm.getPassword()), "nazwa", "adres", auth));
        }
        return true;
    }
}