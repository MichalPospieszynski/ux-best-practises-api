package com.faktury.Cart;

import com.faktury.User.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface CartDetailRepository extends JpaRepository<CartDetail, Long> {
    List<CartDetail> findByUser(User user);
}