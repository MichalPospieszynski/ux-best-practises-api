package com.faktury.Cart;


import com.faktury.Product.Product;
import com.faktury.User.User;

import javax.persistence.*;

@Entity
@Table(name = "cart_detail")
public class CartDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "ctd_id")
    private Long id;

    @Column(name = "ctd_amount")
    private Long amount;

    @ManyToOne
    @JoinColumn(name = "ctd_product_id")
    private Product product;

    @ManyToOne
    @JoinColumn(name = "ctd_user_id")
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}