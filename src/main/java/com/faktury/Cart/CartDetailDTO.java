package com.faktury.Cart;

import com.faktury.Product.ProductDTO;

public class CartDetailDTO {

    private ProductDTO productDTO;
    private long amount;

    public ProductDTO getProductDTO() {
        return productDTO;
    }

    public CartDetailDTO setProductDTO(ProductDTO productDTO) {
        this.productDTO = productDTO;
        return this;
    }

    public long getAmount() {
        return amount;
    }

    public CartDetailDTO setAmount(long amount) {
        this.amount = amount;
        return this;
    }
}
