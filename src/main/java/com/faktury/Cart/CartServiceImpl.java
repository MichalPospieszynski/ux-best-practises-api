package com.faktury.Cart;

import com.faktury.Product.ProductService;
import com.faktury.User.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class CartServiceImpl implements CartService {

    @Autowired
    private CartDetailRepository cartDetailRepository;
    @Autowired
    private ProductService productService;

    @Override
    public Set<CartDetailDTO> findByUser(User user) {
        List<CartDetail> cartDetails = cartDetailRepository.findByUser(user);
        return cartDetails.stream().map(this::map).collect(Collectors.toSet());
    }

    private CartDetailDTO map(CartDetail cartDetail) {
        return new CartDetailDTO()
                .setProductDTO(productService.mapToProductDTO(cartDetail.getProduct()))
                .setAmount(cartDetail.getAmount());
    }
}