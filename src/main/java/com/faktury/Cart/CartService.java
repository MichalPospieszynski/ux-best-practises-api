package com.faktury.Cart;


import com.faktury.User.User;

import java.io.IOException;
import java.util.Set;

public interface CartService {
    Set<CartDetailDTO> findByUser(User user);
}