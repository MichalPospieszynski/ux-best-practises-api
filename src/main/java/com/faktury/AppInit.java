package com.faktury;

import com.faktury.User.Authority;
import com.faktury.User.AuthorityRepository;
import com.faktury.User.User;
import com.faktury.User.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AppInit implements ApplicationListener<ContextRefreshedEvent> {
    private final UserRepository userRepository;
    private final AuthorityRepository authorityRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public AppInit(UserRepository userRepository, AuthorityRepository authorityRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.authorityRepository = authorityRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        List<Authority> authorities = authorityRepository.findAll();
        if (authorities.isEmpty()) {
            Authority auth = authorityRepository.save(new Authority("ROLE_ADMIN"));
            Authority userAuth = authorityRepository.save(new Authority("ROLE_USER"));
            userRepository.save(new User("user@aa.aa", passwordEncoder.encode("password"), "nazwa", "adres", auth));
        }
    }
}