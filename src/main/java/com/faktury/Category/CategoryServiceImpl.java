package com.faktury.Category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;


    @Override
    public Set<CategoryDTO> findAll() {
        return categoryRepository.findAll().stream().map(this::mapToCategoryDTO).collect(Collectors.toSet());
    }

    @Override
    public CategoryDTO mapToCategoryDTO(Category category) {
        return new CategoryDTO()
                .setId(category.getId())
                .setName(category.getName())
                .setParentId(category.getParent() != null ? category.getParent().getId() : null);
    }
}