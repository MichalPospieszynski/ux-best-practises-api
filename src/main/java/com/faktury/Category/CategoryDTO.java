package com.faktury.Category;

public class CategoryDTO {
    private Long id;
    private String name;
    private boolean visible = false;
    private Long parentId;

    public Long getId() {
        return id;
    }

    public CategoryDTO setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public CategoryDTO setName(String name) {
        this.name = name;
        return this;
    }

    public boolean isVisible() {
        return visible;
    }

    public CategoryDTO setVisible(boolean visible) {
        this.visible = visible;
        return this;
    }

    public Long getParentId() {
        return parentId;
    }

    public CategoryDTO setParentId(Long parentId) {
        this.parentId = parentId;
        return this;
    }
}
