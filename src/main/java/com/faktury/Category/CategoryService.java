package com.faktury.Category;


import java.util.Set;

public interface CategoryService {
    Set<CategoryDTO> findAll();

    CategoryDTO mapToCategoryDTO(Category category);
}